
# Indiefy | EJCM

  

Indiefy é um projeto de marketplace em forma de aplicativo focado no compartilhamento, compra e venda de músicas de artistas independentes. 

O sistema foi desenvolvido como Projeto Final do Processo Seletivo 2023.1 da EJCM e teve de sua concepção à entrega final o período de 3 semanas. 

  

**Status do Projeto** : Finalizado

  

  

![Badge](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)

![Badge](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)

  

  

## Tabela de Conteúdo

 
  

1. [Tecnologias utilizadas](#tecnologias-utilizadas)

  

2. [Download](#Download)

  
  

3. [Instalação](#Instalação)

  

4. [Configurações](#Configurações)

  

5. [Execução](#Execução)

  

6. [Arquitetura](#arquitetura)

  

7. [Autores](#autores)

  


  

## Tecnologias utilizadas

  

  

Essas são as frameworks e ferramentas que você precisará instalar para desenvolver esse projeto:

  

  

**[Node.js](https://nodejs.org/en/)**

**[React](https://pt-br.reactjs.org/)**

**[Expo](https://expo.dev/)**

**[Sequelize](https://sequelize.org/)**

  

  

## Download

  

*Para que seja possível a execução dos arquivos deste repositório, o usuário deve clonar através da ferramenta **[git](https://git-scm.com/downloads)**. Abrindo o terminal do seu sistema operacional ou o GitBash, insira o seguinte comando na pasta desejada:*

  

  

``` git

git clone https://gitlab.com/ejcm/indiefy-squad-2.git

```



  

## Instalação

  

Para o correto funcionamento do aplicativo, terão que ser feitas as instalações das dependências, tanto da pasta `back-end`, quanto da pasta `front-end`. Para isso entre na pasta que foi clonada pelo comando e exclua a pasta `.git`:

  

``` bash

cd  indiefy-squad-2

rm -r  .git

```

  

### Na pasta `back`

  

Abra o seu terminal e execute o comando para instalar as dependências da pasta de back-end do projeto denominada `back`.

  

``` bash

cd  back

npm install

```

  

### Na pasta `front`

  

Agora, a partir do passo anterior, execute os comandos abaixo para instalar as dependências da pasta de front-end do projeto denominada `front`.

  

``` bash

cd  ..

cd  front/indiefy-squad-2/

yarn add

```

  

## Configurações

  

Após a instalação, algumas preparações anteriores devem ser realizadas na pasta `back`.

  

A partir dos comandos abaixo, será feita a configuração da pasta `back-end`:

  

```bash

cd  ..

cd  back

cp .env_example  .env

npm run  keys

npm run  migrate

npm run  seed

```



  

## Execução

  

Ainda na pasta `back`, execute o seguinte comando para servir o aplicativo em um servidor customizado para posterior execução no front-end:

  

``` bash

npm run  dev

```

  

Com as configurações feitas, mude a seguir para a pasta `front`, para a execução do aplicativo utilizando o **Expo** utilizando os seguintes comandos:

  

``` bash

cd  ..

cd  front-end/indiefy-squad-2/

expo start

  

```

  

Para parar a execução do aplicativo, basta executar o comando `CTRL + C` no terminal.



## Arquitetura

  

- [Pesquisa Desk](https://www.figma.com/file/6MXEry0Yx5OsotGgV89Auh/Marketplace-Colecionaveis?node-id=0:1)

 - [Prototipagem](https://www.figma.com/file/cQdeZcohhEa88iEd3S4Sv1/Prototype-Squad-2?node-id=0-1&t=CTJ0VjpJTsOMy3hI-0)


- [Trello](https://trello.com/b/9tWW3nN2/squad-2)

  


  ![enter image description here](https://cdn.discordapp.com/attachments/1067468539063189542/1086125618829197322/modelagem-indiefy.png)

  

## Autores

  
  

* Gerente - Débora Vinagre

* Gerente - Vitória Serafim

* Tech Lead - Pedro Moussa

* Dev Front-end - Fernanda Beltrão

* Dev Front-end - Iago Cesar

* Dev Back-end - Leonardo Fonseca

* Dev Back-end - Lorenna Cunha

  


<hr>

  

### Última atualização: 17/03/2023