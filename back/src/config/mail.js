const nodemailer = require('nodemailer');
require("../config/dotenv")();
const fs = require("fs");

const remetente = process.env.MAIL_USER;
const senha = process.env.MAIL_PASSWORD;

const mailer = nodemailer.createTransport({
    service: "gmail",

    auth: {
        user: remetente,
        pass: senha
    }
});

const readHtmlFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html){
        if(err) {
            throw err;
        }
        else {
            callback(null, html);
        }
    });
};

module.exports = {
    mailer,
    readHtmlFile
};