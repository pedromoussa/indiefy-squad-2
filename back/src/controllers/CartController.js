const Cart = require('../models/Cart')
const User = require('../models/User');
const Music = require('../models/Music');


async function create (req, res) {
    try {
        const cart = await Cart.create(req.body);
        return res.status(201).json({cart});
    } catch(err) {
        return res.status(500).json(err);
    }
}

async function index (req, res){
    try {
        const carts = await Cart.findAll();
        return res.status(200).json({message: "Todas as Músicas listadas", Carts: carts});
    } catch (error) {
        return res.status(500).json(error);
    }
};

async function show (req,res){
    const {id} = req.params;
    try {
        const cart = await Cart.findByPk(id);
    
        return res.status(200).json({message: "Música encontrada", Cart: cart});
    } catch (error) {
        return res.status(500).json(error);
    }
};

async function update (req,res){
    const {id} = req.params;
    try {
        const [updated] = await Cart.update(req.body, {where: {id: id}});
        if(updated){
            const cart = await Cart.findByPk(id);
            return res.status(200).json(cart);
        }
        throw new Error();
    } catch (error) {
        return res.status(500).json("Música não encontrada");
    }
}

async function destroy (req,res){
    const {id} = req.params;
    try {
        const deleted = await Cart.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Música deletada");
        }
    
        throw new Error();
    
    } catch (error) {
        return res.status(500).json("Música não encontrada");
    }
}

async function addToCart (req, res) {
    const {userId, musicId} = req.params;
    try{
      const user = await User.findByPk(userId);
      const music = await Music.findByPk(musicId);

      const constraints = {
        UserId: userId,
        MusicId: musicId
      }

      const cart = await Cart.create(constraints);
 
      const atributes = {
          price: music.price, 
      }
         
      await Cart.update(atributes, {where: {UserId: userId, MusicId: musicId}});
      
      return res.status(200).json({msg: "Música adicionada ao carrinho"});

    } catch (error) {
        return res.status(500).json({error});
    }
}

async function removeFromCart (req, res) {
    const {userId, musicId} = req.params;
    try{

        const cart = await Cart.findOne({where: {UserId: userId, MusicId: musicId}});
        await cart.destroy();
        return res.status(200).json({msg: "Música removida do carrinho"});
    
    } catch (error) {
        return res.status(500).json({error});
    }
}

async function listCart(req, res){
    const {userId} = req.params;
    try {

        const cart = await Cart.findAll({where: {UserId: userId}});
        let musicIds = new Array();

        cart.forEach((e) => {
            musicIds.push(e.MusicId);
        });

        let musics = new Array();
        while(musicIds.length > 0) {
            musics.push(await Music.findByPk(musicIds.shift()));
        }
        
        return res.status(200).json(musics);

    } catch (error) {
        return res.status(500).json({error});
        
    }
}

module.exports = {
    create,
    index, 
    show,
    update,
    destroy,
    addToCart,
    removeFromCart,
    listCart
}



