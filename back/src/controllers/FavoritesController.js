const Favorites = require('../models/Favorites');
const Music = require('../models/Music');
const User = require('../models/User');

async function index (req, res){
    const {userId} = req.params;
    try {

        const favorites = await Favorites.findAll({where: {UserId: userId}});
        let musicIds = new Array();

        favorites.forEach((e) => {
            musicIds.push(e.MusicId);
        });

        let musics = new Array();
        while(musicIds.length > 0) {
            musics.push(await Music.findByPk(musicIds.shift()));
        }
        
        return res.status(200).json(musics);

    } catch (error) {
        return res.status(500).json({error});
        
    }
};

async function removeFromFavorites (req,res){
    const {userId, musicId} = req.params;
    try{

        const favorites = await Favorites.findOne({where: {UserId: userId, MusicId: musicId}});
        await favorites.destroy();
        return res.status(200).json({msg: "Removida dos Favoritos!"});
    
    } catch (error) {
        return res.status(500).json({error});
    }
}

async function addToFavorites(req, res) {
    const {userId, musicId} = req.params;
    try{
      const user = await User.findByPk(userId);
      const music = await Music.findByPk(musicId);

      const constraints = {
        UserId: userId,
        MusicId: musicId
      }

      const favorites = await Favorites.create(constraints);
         
      await Favorites.update(req.body, {where: {UserId: userId, MusicId: musicId}});
      
      return res.status(200).json({msg: "Música favoritada!"});

    } catch (error) {
        return res.status(500).json({error});
    }
}


module.exports = {

    index,
    removeFromFavorites,
    addToFavorites

}



