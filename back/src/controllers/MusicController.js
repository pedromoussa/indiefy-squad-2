const Music = require('../models/Music')
const Photo = require('../models/Photo');
const path = require('path');
const fsPromise = require('fs').promises;

async function create (req, res) {
    try {
        const music = await Music.create(req.body);
        return res.status(201).json({message: "Música cadastrada com sucesso", Music: music});
    } catch(err) {
        return res.status(500).json(err);
    }
}

async function index (req, res){
    try {
        const musics = await Music.findAll();
        return res.status(200).json({message: "Todas as músicas listadas", Musics: musics});
    } catch (error) {
        return res.status(500).json(error);
    }
};

async function show (req,res){
    const {id} = req.params;
    try {
        const music = await Music.findByPk(id);
    
        return res.status(200).json({message: "Música encontrada", Music: music});
    } catch (error) {
        return res.status(500).json(error);
    }
};

async function update (req,res){
    const {id} = req.params;
    try {
        const [updated] = await Music.update(req.body, {where: {id: id}});
        if(updated){
            const music = await Music.findByPk(id);
            return res.status(200).json(music);
        }
        throw new Error();
    } catch (error) {
        return res.status(500).json("Música não encontrada");
    }
}

async function destroy (req,res){
    const {id} = req.params;
    try {
        const deleted = await Music.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Música deletada");
        }
    
        throw new Error();
    
    } catch (error) {
        return res.status(500).json("Música não encontrada");
    }
}

async function addMusicPhoto (req, res){
    const {id} = req.params;

    try{
        const music = await Music.findByPk(id, {include:{model: Photo}});
        if(req.file){
            const path = process.env.APP_URL + "/uploads/" + req.file.filename;
            const photo = await Photo.create({
                path: path,

            });
            await music.setPhoto(photo);
        }
        const musicUpdated = await Music.findByPk(id, {include:{model: Photo}});
        return res.status(200).json(musicUpdated);

    } catch(error) {
        return res.status(500).json({error});
    }
}

async function deleteMusicPhoto(req, res){
    const {id} = req.params;
    try{
        const photo = await Photo.findByPk(id);
        const pathDb = photo.path.split("/").slice(-1)[0]
        await fsPromise.unlink(path.join(__dirname,'..', '..', 'uploads', pathDb));
        await photo.destroy ();
        return res.status(200).json("Foto removida com sucesso");
    } catch (error) {
        return res.status(500).json({error});
    }
} 

module.exports = {
    create,
    index, 
    show,
    update,
    destroy,
    addMusicPhoto,
    deleteMusicPhoto
}



