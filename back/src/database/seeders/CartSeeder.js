const Cart = require("../../models/Cart");
const faker = require('faker-br');

const seedCart = async function () {
    try{
        await Cart.sync({ force: true });

        for (let i = 0; i < 10; i++){
            await Cart.create({

                quantity: faker.random.number(),
                price: faker.commerce.price(),
                payment: faker.finance.accountName(),
                MusicId: i+1,
                UserId: i+1,
            });
        }
    } catch (err) {
        console.log(err);
    }
};

module.exports = seedCart;