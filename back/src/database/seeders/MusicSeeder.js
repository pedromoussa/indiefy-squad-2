const Music = require("../../models/Music");
const faker = require('faker-br');

const seedMusic = async function () {
    try{
        await Music.sync({ force: true });

        for (let i = 0; i < 10; i++){
            await Music.create({

                name: faker.lorem.words(),
                price: faker.commerce.price(),
                genre: faker.random.word(),
                art: faker.image.abstract(),
            });
        }
    } catch (err) {
        console.log(err);
    }
};

module.exports = seedMusic;