const User = require("../../models/User");
const faker = require('faker-br');

const seedUser = async function () {
    try{
        await User.sync({ force: true });

        for (let i = 0; i < 10; i++){
            await User.create({

                userName: faker.name.firstName(),
                email: faker.internet.email(),
                cpf: faker.br.cpf(),
                password: faker.internet.password(),
                birthDate: "2001-01-11",
                profilePhoto: faker.image.abstract(),
                hash: "212121",
                salt: "aaaaa",
            });
        }
    } catch (err) {
        console.log(err);
    }
};

module.exports = seedUser;