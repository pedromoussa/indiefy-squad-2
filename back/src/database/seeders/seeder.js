require('../../config/dotenv')();
require('../../config/sequelize');

const seedUser = require('./UserSeeder');
const seedMusic = require('./MusicSeeder');
const seedComment = require('./CommentSeeder');
const seedCart = require('./CartSeeder');

(async () => {
  try {
    
    await seedUser();
    await seedCart();
    await seedComment();
    await seedMusic();

  } catch(err) { console.log(err) }
})();
