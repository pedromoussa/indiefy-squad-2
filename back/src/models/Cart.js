const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Cart = sequelize.define('Cart', {

    price: {
        type: DataTypes.DOUBLE,
        allowNull: true
    },

    payment: {
        type: DataTypes.STRING,
        allowNull: true
   },

   id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
   }
});


// exportando Model
module.exports = Cart;