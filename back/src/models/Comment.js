const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Comment = sequelize.define('Comment', {
    text: {
        type: DataTypes.STRING,
        allowNull: true
    },
});

//Exportando Model
module.exports = Comment;