const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Music = sequelize.define('Music', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },

    genre: {
        type: DataTypes.STRING,
        allowNull: false
    },

    art: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
});

// Relacionamento
Music.associate = function (models){
    Music.belongsTo(models.User);
    
    Music.hasOne(models.Photo);

    Music.belongsToMany(models.User, {
        through: models.Cart,
        constraints: false
    });

    Music.belongsToMany(models.User, {
        through: models.Favorites,
        as: 'Favorited',
        constraints: false
    });

    Music.belongsToMany(models.User, {
        through: models.Comment,
        constraints: false
    });
};

//Exportando Model
module.exports = Music;