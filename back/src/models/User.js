const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    userName: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    birthDate: {
        type: DataTypes.DATEONLY,
        allowNull: true
    },

    profilePhoto: {
        type: DataTypes.STRING,
        allowNull: false
    },

    password: {
        type: DataTypes.STRING,
        allowNull: false
    },

    hash: {
        type: DataTypes.STRING,
        allowNull: false
    },

    salt: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
});
    

// Relacionamentos
User.associate = function (models) {
    User.hasMany(models.Music);

    User.hasOne(models.Photo)

    User.belongsToMany(models.Music, {
        through: models.Favorites,
        constraints: false
    });

    User.belongsToMany(models.Music, {
        through: models.Comment,
        constraints: false
    });

    User.belongsToMany(models.Music, {
        through: models.Cart,
        constraints: false
    });

    User.belongsToMany(models.User, {
        through: "Follow", 
        as: "following",
        foreignKey: "followingId"
    })

    User.belongsToMany(models.User, {
        through: "Follow", 
        as: "followed",
        foreignKey: "followedId"
    })
};

//exportando Model
module.exports = User;