const Express = require('express');
const router = Express();


const UserController = require('../controllers/UserController');
const MusicController = require('../controllers/MusicController');
const FavoritesController = require('../controllers/FavoritesController');
const CommentController = require('../controllers/CommentController');
const CartController = require('../controllers/CartController');

const path = require("path");
const multer = require("multer");
const storage = require("../config/files");

const upload = multer({
    storage: storage, 
    fileFilter: function(req, file, cb){
        const ext = path.extname(file.originalname);
        if(ext !== ".png" && ext !== ".jpg" && ext !== ".jpeg"){
            return cb(new Error ("erro: extensão não suportada"), false);
        }
        cb(null, true)
    },
    limits: {
        fileSize: 2048*2048
    }
});

router.post('/User/addUserPhoto/Photo/:id', upload.single('path'), UserController.addUserPhoto);
router.post('/Music/addMusicPhoto/Photo/:id', upload.single('path'), MusicController.addMusicPhoto);
router.delete('/deleteUserPhoto/Photo/:id', UserController.deleteUserPhoto);
router.delete('/deleteMusicPhoto/Photo/:id', MusicController.deleteMusicPhoto);


//rotas para User
router.post('/User', UserController.create);
router.get('/User/:id', UserController.show);
router.get('/User', UserController.index);
router.put('/User/:id', UserController.update);
router.delete('/User/:id', UserController.destroy);
router.put('/follow/User/:id', UserController.follow);
router.delete('/unfollow/User/:id', UserController.unfollow);

//rotas para Cart
router.post('/Cart', CartController.create);
router.get('/Cart/:id', CartController.show);
router.get('/Cart', CartController.index);
router.put('/Cart/:id', CartController.update);
router.delete('/Cart/:id', CartController.destroy);
router.put('/addToCart/User/:userId/Music/:musicId', CartController.addToCart);
router.delete('/removeFromCart/User/:userId/Music/:musicId', CartController.removeFromCart);
router.get('/listCart/User/:userId', CartController.listCart);

//rotas para Music
router.post('/Music', MusicController.create);
router.get('/Music/:id', MusicController.show);
router.get('/Music', MusicController.index);
router.put('/Music/:id', MusicController.update);
router.delete('/Music/:id', MusicController.destroy);

//rotas para Comment
router.post('/Comment', CommentController.create);
router.get('/Comment/:id', CommentController.show);
router.get('/Comment/Music/:musicId', CommentController.index);
router.put('/Comment/:id', CommentController.update);
router.delete('/Comment/User/:userId/Music/:musicId', CommentController.destroy);
router.put('/addComment/Music/:musicId/User/:userId', CommentController.addComment);

//rotas para Favorites
router.get('/Favorites/User/:userId', FavoritesController.index);
router.put('/addToFavorites/User/:userId/Music/:musicId', FavoritesController.addToFavorites);
router.delete('/removeFromFavorites/User/:userId/Music/:musicId', FavoritesController.removeFromFavorites);


module.exports = router;
