import styled from 'styled-components/native';
import Router from './source/routes/router';

const AppDiv = styled.View `
    display: flex;
    margin: 0;
    padding: 0;
`;

export default function App() {
    return (
        <AppDiv>
            <Router/>
        </AppDiv>
    );
}
