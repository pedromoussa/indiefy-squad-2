import '@fontsource/poppins';
import '@fontsource/bebas-neue';
import '@fontsource/be-vietnam';

const profileIcon = require('./assets/profileIcon.svg');
const songFile = require('./assets/songFile.svg');

// LETREIROS
const letLogo = require('./assets/LetLogo.svg');
const logo = require('./assets/Logo.svg');
const letreiro = require('./assets/Letreiro.svg');

// Placeholder para as capas
const coverPlaceholder = require('./assets/capaPlaceholder.svg');

// BIBLIOTECA
const lib = require('./assets/biblioteca/biblioteca.svg');
const libSelect = require('./assets/biblioteca/bibliotecado.svg');

// BUSCA
const search = require('./assets/busca/buscar.svg');
const searchSelect = require('./assets/busca/buscado.svg');

// FAVORITOS
const fav = require('./assets/favorito/favoritar.svg');
const favorited = require('./assets/favorito/favoritado.svg');

// HOME
const home = require('./assets/inicio/home.svg');
const homeSelect = require('./assets/inicio/homed.svg');

// NOTIFICAÇÕES
const notif = require('./assets/notificacoes/notificacao.svg');
const notifSelect = require('./assets/notificacoes/notificado.svg');

// PAGAMENTO
const card = require('./assets/pagamento/Vector.svg');
const pix = require('./assets/pagamento/ic_baseline-pix.svg');
const gift = require('./assets/pagamento/Vector-1.svg');

// SACOLA
const cart = require('./assets/sacola/sacola.svg');
const carted = require('./assets/sacola/sacolado.svg') 


export const global = {
    colors: {
        laranja: '#FF420F',
        azul: '#00249E',
        rosapink: '#FF00A8',
        rosa: '#FF9BB9',
        verde: '#169B00'
    },
    fonts: {
        poppins: 'Poppins',
        bebas: 'Bebas Neue',
        vietnam: 'Be Vietnam'
    },
    icons: {
        logo: logo,
        letLogo: letLogo,
        letreiro: letreiro,

        coverPlaceholder: coverPlaceholder,

        library: lib,
        libSelect: libSelect,

        notif: notif,
        notifSelect,

        home: home,
        homeSelect: homeSelect,

        fav: fav,
        faved: favorited,

        cart: cart,
        addedCart: carted,

        search: search,
        searchSelect: searchSelect,
    }
};
