import { Animated } from "react-native";
import { LoadBar, LoadDiv } from "./style";

function Animation(start:number, end:number, duration: number, delay?:number) {
    const valor = new Animated.Value(20);

    setTimeout(() => {
        Animated.loop(
            Animated.sequence([
                Animated.timing(
                    valor, {
                        toValue: end,
                        duration: duration*1000, useNativeDriver:true
                    }
                ),
                Animated.timing(
                    valor, {
                        toValue: start,
                        duration: (duration*1000), useNativeDriver:true
                    }
                )
            ]),
        ).start();
    }, (delay? delay*1000:0));
    
    const repeat = valor.interpolate({
        inputRange: [start, end],
        outputRange: [`${start}%`, `${end}%`]
    })

    return repeat;
} 

export default function Loading() {

    const zero = Animation(20, 100, 0.8);
    const dois = Animation(20, 100, 0.8, 0.2);
    const quatro = Animation(20, 100, 0.8, 0.4);
    const cinco = Animation(20, 100, 0.8, 0.5);

    return (
        <LoadDiv>
            <LoadBar style={{ height: dois}}/>
            <LoadBar style={{ height: quatro}}/>
            <LoadBar style={{ height: cinco}}/>
            <LoadBar style={{ height: quatro}}/>
            <LoadBar style={{ height: dois}}/>
            <LoadBar style={{ height: zero}}/>
            <LoadBar style={{ height: dois}}/>
            <LoadBar style={{ height: quatro}}/>
            <LoadBar style={{ height: cinco}}/>
            <LoadBar style={{ height: quatro}}/>
            <LoadBar style={{ height: dois}}/>
            <LoadBar style={{ height: zero}}/>
            <LoadBar style={{ height: dois}}/>
            <LoadBar style={{ height: quatro}}/>
            <LoadBar style={{ height: cinco}}/>
            <LoadBar style={{ height: quatro}}/>
        </LoadDiv>
    );
}