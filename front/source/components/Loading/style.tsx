import styled from "styled-components/native";
import { global } from "../../GlobalStyle";
import { Animated } from "react-native";

export const LoadDiv = styled.View `
    display: flex;
    flex-direction: row;
    width: 80vw;
    height: 12vh;

    align-items: center;
    justify-content: space-between;
`;

export const LoadBar = styled(Animated.View) `
    display: block;
    width: 3.5%;
    height: 100%;
    background-image: linear-gradient(${global.colors.laranja}, ${global.colors.rosapink}, ${global.colors.azul});
    border-radius: 5vh;
`;

