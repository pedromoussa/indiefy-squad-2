import { useNavigation } from "@react-navigation/native";
import { PrevAuthor, PrevCover, PrevInfo, PrevTitle,
    PreviewDiv, Price, SongAlbum } from "./style";

const Cover = require('../../assets/capaPlaceholder.svg');

type musicInfo = {
    cover?: any
    title?: string;
    author?: string;
    album?: boolean;
    price?: number;
    chosen?: boolean;
}
export default function MusicPreview(
    {cover, title, author, album, price, chosen}:musicInfo
) {

    const nav = useNavigation();

    return (
        <PreviewDiv onPress={() => nav.navigate('Product' as never)}>
            <PrevCover source={Cover}/>
            <PrevTitle>Dawn FM{title}</PrevTitle>
            <PrevAuthor>The Weeknd{author}</PrevAuthor>

            <PrevInfo>
                <SongAlbum style={{color: chosen?'#FF00A8':''}}>{`${album? 'Álbum':'Música'}`}</SongAlbum>
                <Price>$10.99{price}</Price>
            </PrevInfo>
        </PreviewDiv>
    );
}