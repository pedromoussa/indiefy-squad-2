import styled from "styled-components/native";
import { global } from "../../GlobalStyle";

export const PreviewDiv = styled.TouchableOpacity `
    display: flex;
    flex-direction: column;
    margin: 0 5.12vw 0 0;
    *{
        margin: 0;
        padding: 0;
    }
`;

export const PrevCover = styled.Image `
    width: 23.84vw;
    height: 23.84vw;
`;

export const PrevTitle = styled.Text `
    margin: 0.3vh 0 0 0.5vw;
    font-family: ${global.fonts.vietnam};
    font-size: 3.07vw;
    font-weight: 500;
    color: white;

`;

export const PrevAuthor = styled.Text `
    margin: 0 0 0 0.5vw;
    font-family: ${global.fonts.vietnam};
    font-size: 2.05vw;
    font-weight: 500;
    color: white;
`;

export const PrevInfo = styled.View `
    display: flex;
    flex-direction: row;
    width: 95%;
    margin: 0.5vh 0 0 0.5vw;
    justify-content: space-between;
`;

export const SongAlbum = styled.Text `
    display: flex;
    font-family: ${global.fonts.poppins};
    font-size: 2.564vw;
    font-weight: 700;
    color: black;
`;

export const Price = styled.Text `
    display: flex;
    font-family: ${global.fonts.vietnam};
    font-size: 2.564vw;
    font-weight: 600;
    color: ${global.colors.verde};
`;