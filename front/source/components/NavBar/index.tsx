import { useState } from "react";
import { IconDiv, IconTitle, NavButton, NavDiv, NavIcon } from "./style";

const library = require('../../assets/biblioteca/biblioteca.svg');
const libSelect = require('../../assets/biblioteca/bibliotecado.svg');

const notific = require('../../assets/notificacoes/notificacao.svg');
const notifSelect = require('../../assets/notificacoes/notificado.svg');

const search = require('../../assets/busca/buscar.svg');
const searchSelect = require('../../assets/busca/buscado.svg');

const homeIcon = require('../../assets/inicio/home.svg');
const homeSelect = require('../../assets/inicio/homed.svg');

export default function NavBar() {

    const [page, tggPage] = useState('home');

    return (
        <NavDiv>
            <IconDiv>
                <NavButton activeOpacity={1.0} onPress={() => tggPage('notif')}>
                    <NavIcon source={page==='notif'? notifSelect:notific}/>
                    <IconTitle style={{color:(page==='notif'?'#FF00A8':'#fff')}}>Notificações</IconTitle>
                </NavButton>

                <NavButton onPress={() => tggPage('lib')}>
                    <NavIcon source={page==='lib'? libSelect:library}/>
                    <IconTitle style={{color:(page==='lib'?'#FF00A8':'#fff')}}>Biblioteca</IconTitle>
                </NavButton>
                
                <NavButton onPress={() => tggPage('search')}>
                    <NavIcon source={page==='search'? searchSelect:search}/>
                    <IconTitle style={{color:(page==='search'?'#FF00A8':'#fff')}}>Buscar</IconTitle>
                </NavButton>
                <NavButton onPress={() => tggPage('home')}>
                    <NavIcon source={page==='home'? homeSelect:homeIcon}/>
                    <IconTitle style={{color:(page==='home'?'#FF00A8':'#fff')}}>Home</IconTitle>
                </NavButton>
            </IconDiv>
        </NavDiv>
    );
}