import styled from "styled-components/native";
import { global } from "../../GlobalStyle";

export const NavDiv = styled.View `
    display: flex;
    width: 100vw;
    height: 19.19vh;

    position: fixed;
    bottom: 0;
    
    background-image: linear-gradient(rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 1));
`;

export const IconDiv = styled.View `
    display: flex;
    flex-direction: row;
    width: 100%;

    bottom: 0;
    margin: auto 0 1vh 0;

    justify-content: space-evenly;
`;

export const NavButton = styled.TouchableOpacity `
    display: flex;
    flex-direction: column;

    align-items: center;

    background: transparent;
    border: none;
`;

export const NavIcon = styled.Image `
    width: 7.94vw;
    height: 7.94vw;
`;

export const IconTitle = styled.Text `
    margin: 0.5vh 0 0 0;
    font-family: ${global.fonts.poppins};
    font-size: 2.05vw;
    font-weight: 500;
    color: white;
`;