import { PlayCover, PlayDiv, PlayTitle } from "./style";

const Cover = require('../../assets/capaPlaceholder.svg');

type playInfo = {
    cover?: any;
    title?: string;
}
export default function Playslist(
    {cover, title}: playInfo
) {
    return (
        <PlayDiv>
            <PlayCover source={Cover}/>
            <PlayTitle>This is Manu{title}</PlayTitle>
        </PlayDiv>
    );
}