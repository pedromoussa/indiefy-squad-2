import styled from "styled-components/native";
import { global } from "../../GlobalStyle";

export const PlayDiv = styled.View `
    display: flex;
    flex-direction: row;
    width: 43.33vw;
    height: 7.7vh;

    align-items: center;

    background: ${global.colors.azul};
    
    border-radius: 2vw;
`;

export const PlayCover = styled.Image `
    width: 5.92vh;
    height: 5.92vh;
    margin: 0 0 0 2vw;

    background: ${global.colors.laranja};

    border-radius: 1.4vw;
`;

export const PlayTitle = styled.Text `
    margin: 0 0 0 2vw;

    font-family: ${global.fonts.poppins};
    font-size: 3.33vw;
    font-weight: 500;

    color: white;
`;