import { BackIcon, PrevButton, PrevDiv } from "./style";

import { useNavigation } from "@react-navigation/native";

const backIcon = require('../../assets/backIcon.svg');

export default function Previous() {

    const nav = useNavigation();
    
    return (
        <PrevDiv>
            <PrevButton onPress={() => nav.goBack()}>
                <BackIcon source={backIcon}/>
            </PrevButton>
        </PrevDiv>
    );
}