import styled from "styled-components/native";

export const PrevDiv = styled.View `
    display: flex;
    position: absolute;
    top: 5vh;
    left: 7vw;
`;

export const PrevButton = styled.TouchableOpacity `
`;

export const BackIcon = styled.Image `
    width: 3.07vw;
    height: 2.36vh;
`;