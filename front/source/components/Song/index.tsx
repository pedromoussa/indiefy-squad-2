import { useState } from "react";
import { FavButton, FavIcon, OptionIcon, SongActions,
    SongAuthor,SongCover, SongDiv, SongInfo,
    SongTitle } from "./style";

const songCover = require('../../assets/capaPlaceholder.svg');
const favoritar = require('../../assets/favorito/favoritar.svg');
const favoritado = require('../../assets/favorito/favoritado.svg');
const optionIcon = require('../../assets/options.svg');

type songInfo = {
    cover?: any;
    title?: string;
    author?: string;
    favorite?: boolean;
}
export default function Song( {cover, title, author}: songInfo ) {
    const [fav, setFav] = useState(favoritar);

    const Favorite = () => {

        if(fav === favoritar) setFav(favoritado);
        else setFav(favoritar);
    }

    return (
        <SongDiv>
            <SongCover source={songCover}/>

            <SongInfo>
                <SongTitle>Não dá mais{title}</SongTitle>
                <SongAuthor>Jovem Dionisio{author}</SongAuthor>
            </SongInfo>

            <SongActions>
                <FavButton onPress={Favorite} >
                    <FavIcon source={fav}/>
                </FavButton>
                <OptionIcon source={optionIcon}/>
            </SongActions>
        </SongDiv>
    );
}