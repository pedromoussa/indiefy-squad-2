import styled from "styled-components/native";
import { global } from "../../GlobalStyle";

export const SongDiv = styled.View `
    display: flex;
    flex-direction: row;
    width: 91.79vw;
    height: 5.92vh;
    align-items: center;
    margin: 0 0 1vh 0;

    * {
        margin: 0;
        padding: 0;
    }
`;

export const SongInfo = styled.View `
    display: column;
    max-width: 61.79vw;
    margin: 0 0 0 2vw;
`;

/*------------------------------------------------------*/

export const SongCover = styled.Image `
    width: 5.92vh;
    height: 5.92vh;
`;

export const SongTitle = styled.Text `
    font-family: ${global.fonts.vietnam};
    font-size: 5.12vw;
    font-weight: 500;
    color: white;
`;

export const SongAuthor = styled.Text `
    font-family: ${global.fonts.vietnam};
    font-size: 3.33vw;
    font-weight: 500;
    color: #ADADAD;
`;

/*------------------------------------------------------*/

export const SongActions = styled.View `
    display: flex;
    flex-direction: row;
    width: 12.3vw;
    right: 0;

    justify-content: space-between;

    margin-left: auto;
`;

export const FavButton = styled.TouchableOpacity `
    display: flex;
`;

export const FavIcon = styled.Image `
    width: 2.36vh;
    height: 2.36vh;
`;

export const OptionIcon = styled.Image `
    width: 0.59vh;
    height: 2.36vh;
`;