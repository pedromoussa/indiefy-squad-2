import { useState } from "react";
import { AddCover, AddCoverTitle, AddProd, AddProdDiv, Arrow, DropDiv, DropItem, Dropdown, ProdCover, SongDiv, SongFile, SongText } from "./styled";

import * as imageSelect from 'expo-image-picker';
import { global } from "../../GlobalStyle";
import Previous from "../../components/Previous";
import { Animated } from "react-native";

export default function AddProduct() {

    const [img, setImg] = useState('');

    const selectImg = async () => {
        const imagem = await imageSelect.launchImageLibraryAsync({
            mediaTypes: imageSelect.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 4],
            quality: 1,
        });
    
        console.log(imagem);
    
        if (!imagem.canceled) setImg(imagem.assets[0].uri);

        return imagem;
    };

/*------------------------------------------------------*/

    const [open, setOpen] = useState(false);
    const [tamanho, setTamanho] = useState(new Animated.Value(0.5));
    const [grau, setGrau] = useState(new Animated.Value(0));

    const [current, setCurrent] = useState(0);
    const options = ['Tipo de Lançamento',
                    'Single (1 faixa)',
                    'EP (2-3 faixas)',
                    'Álbum (4+ faixas)'];

    const OpenOrClose = (option?: number) => {
        if(option) setCurrent(option);

        if(open) {
            Animated.timing(
                tamanho, {
                    toValue: 0.5,
                    duration: 500, useNativeDriver:true
                }
            ).start();

            Animated.timing(
                grau, {
                    toValue: 0,
                    duration: 500,
                    useNativeDriver: true
                }
            ).start();

            setOpen(false);
        }
        else {
            Animated.timing(
                tamanho, {
                    toValue: 18,
                    duration: 500, useNativeDriver:true
                }
            ).start();

            Animated.timing(
                grau, {
                    toValue: 180,
                    duration: 500,
                    useNativeDriver: true
                }
            ).start();
            console.log(tamanho);
            
            setOpen(true);
        }
    }
    
    const rotacao = grau.interpolate({
        inputRange: [0, 180],
        outputRange: ['0deg', '180deg']
    });

    const altura = tamanho.interpolate({
        inputRange: [0.5, 18],
        outputRange: ['0.5vh', '18vh']
    });

/*------------------------------------------------------*/

    return (
        <AddProdDiv>
            <Previous/>
            <AddCover onPress={selectImg}>
                {img? <ProdCover source={{uri: img}}/>:<ProdCover source={global.icons.coverPlaceholder}/>}
                <AddCoverTitle>{img?`Trocar Capa`:`+ Adicionar Capa`}</AddCoverTitle>
            </AddCover>

            <Dropdown>
                <Arrow
                style={{transform: [{rotate: rotacao}]}}
                source={require('../../assets/arrowDown.svg')}/>

                <DropItem style={{color: (current===0)?'#636363':'#fff'}}
                onPress={() => OpenOrClose()}
                >{`${options[current]}`}</DropItem>

                <DropDiv style={{height: altura}}>
                    <DropItem onPress={() => OpenOrClose(1)}>Single (1 faixa)</DropItem>
                    <DropItem onPress={() => OpenOrClose(2)}>EP (2-3 faixas)</DropItem>
                    <DropItem onPress={() => OpenOrClose(3)}>Álbum (4+ faixas)</DropItem>
                </DropDiv>
            </Dropdown>

            <SongDiv>
                <SongFile source={require('../../assets/songFile.svg')}/>
                <SongText>+ Adicionar Arquivos</SongText>
            </SongDiv>

            <AddProd>Adicionar faixa ao perfil</AddProd>
        </AddProdDiv>
    );
}