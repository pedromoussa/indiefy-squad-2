import styled from "styled-components/native";
import { global } from "../../GlobalStyle";
import { Animated } from "react-native";

export const AddProdDiv = styled.View `
    display: column;
    width: 100vw;
    min-height: 100vh;

    align-items: center;

    background: black;
`;

export const AddCover = styled.TouchableOpacity `
    display: flex;
    align-items: center;
    margin: 7vh 0 0 0;
`;

export const AddCoverTitle = styled.Text `
    margin: 2vh 0 0 0;
    font-family: ${global.fonts.poppins};
    font-weight: 500;
    font-size: 3.5vw;
    color: white;
`;

export const ProdCover = styled.Image `
    width: 57.69vw;
    height: 57.69vw;
    margin: 5vh 0 0 0;
`;

/*------------------------------------------------------*/

export const Dropdown = styled.View `
    display: flex;
    width: 65vw;

    margin: 10vh 0 0 0;

    justify-content: center;
`;

export const Arrow = styled(Animated.Image) `
    display: flex;
    width: 3.5vw;
    height: 1vh;

    position: absolute;
    right: 1vw;
    top: 3vh;
`;

export const DropDiv = styled(Animated.View) `
    width: 100%;
    height: 0.5vh;

    overflow: hidden;

    background: ${global.colors.laranja};

    border: none;
    border-radius: 2vw;
`;

export const DropItem = styled.Text `
    margin: 0 0 1vh 2vw;
    padding: 2vh 0 0 0;
    
    font-family: ${global.fonts.poppins};
    font-weight: 500;
    font-size: 4vw;
    
    color: white;
`;

/*------------------------------------------------------*/

export const SongDiv = styled.View `
    display: flex;
    flex-direction: row;

    margin: 7vh;

    align-items: center;
`;

export const SongFile = styled.Image `
    width: 15vw;
    height: 14.5vw;
`;

export const SongText = styled.Text `
    font-family: ${global.fonts.poppins};
    font-weight: 600;
    font-size: 4vw;
    
    color: ${global.colors.laranja};
`;

/*------------------------------------------------------*/

export const AddProd = styled.Text `
    padding: 1vh 3vw;
    margin: 0 0 5vh 0;

    font-family: ${global.fonts.poppins};
    font-weight: 500;
    font-size: 4vw;
    
    color: white;
    background: ${global.colors.rosapink};
    border-radius: 2vw;
`;