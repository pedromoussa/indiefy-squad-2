import { ActionDiv, Actions, HomeDiv, Logo,
    PlayGrid, ProfileIcon, SacolaIcon, SuggItems,
    SectionTitle, Suggested, Recently, ChosenSec,
    ChosenItems } from "./style";

import MusicPreview from "../../components/MusicPreview";
import Playslist from "../../components/Playlist";
import Song from "../../components/Song";
import NavBar from "../../components/NavBar";
import { TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

const logoIndiefy = require('../../assets/Logo.svg');
const sacola = require('../../assets/sacola/sacola.svg');
const profileIcon = require('../../assets/profileIcon.svg');

export default function Home() {

    const nav = useNavigation();

    return (
        <HomeDiv>
            <ActionDiv>
                <Logo source={logoIndiefy}/>
                <Actions>
                    <SacolaIcon source={sacola}/>
                    <TouchableOpacity onPress={() => nav.navigate('Login' as never)}>
                        <ProfileIcon source={profileIcon}/>
                    </TouchableOpacity>
                </Actions>
            </ActionDiv>

            <PlayGrid>
                <Playslist/>
                <Playslist/>
                <Playslist/>
                <Playslist/>
            </PlayGrid>

            <Suggested>
                <SectionTitle style={{marginLeft: '4.1vw'}}>Recomendados</SectionTitle>
                <SuggItems horizontal={true} showsHorizontalScrollIndicator={false}>
                    <MusicPreview/>
                    <MusicPreview/>
                    <MusicPreview/>
                    <MusicPreview/>
                    <MusicPreview/>
                    <MusicPreview/>
                </SuggItems>
            </Suggested>

            <Recently>
                <SectionTitle>Ouvidos recentemente</SectionTitle>
                <Song/>
                <Song/>
                <Song/>
            </Recently>

            <ChosenSec>
                <SectionTitle>Escolhidos pelo Indiefy</SectionTitle>
                <ChosenItems>
                    <MusicPreview chosen/>
                    <MusicPreview chosen/>
                    <MusicPreview chosen/>
                </ChosenItems>
            </ChosenSec>

            <NavBar/>
        </HomeDiv>
    );
}