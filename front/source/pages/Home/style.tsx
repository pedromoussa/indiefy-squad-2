import styled from 'styled-components/native';
import { global } from "../../GlobalStyle";

export const HomeDiv = styled.View `
    display: inline-flex;
    flex-direction: column;
    width: 100vw;

    align-items: center;

    background: black;
`;

export const SectionTitle = styled.Text `
    margin: 0 0 1vh 0;
    font-family: ${global.fonts.poppins};
    font-size: 4.61vw;
    font-weight: 600;
    color: white;
`;

/*------------------------------------------------------*/

export const ActionDiv = styled.View `
    display: flex;
    flex-direction: row;
    width: 91.79vw;

    margin: 2vh 0 2.84vh 0;

    justify-content: space-between;
    align-items: center;
`;

export const Logo = styled.Image `
    width: 49.23vw;
    height: 8.29vh;
`;

export const Actions = styled.View `
    display: flex;
    flex-direction: row;
    width: 21.79vw;
    align-items: center;
    justify-content: space-between;
`;

export const ProfileIcon = styled.Image `
    width: 3.91vh;
    height: 3.91vh;
`;

export const SacolaIcon = styled.Image `
    display: flex;
    width: 2.6vh;
    height: 2.6vh;
`;

/*------------------------------------------------------*/

export const PlayGrid = styled.View `
    display: grid;
    grid-template-columns: auto auto;
    row-gap: 1.18vh;
    justify-content: space-between;

    width: 91.79vw;
    margin: 0 0 4.5vh 0;
`;

/*------------------------------------------------------*/

export const Suggested = styled.View `
    display: flex;
    flex-direction: column;
    width: 100%;

    padding: 1vh 0 0 0;

    background: ${global.colors.laranja};

    border-radius: 2.564vw;
`;

export const SuggItems = styled.ScrollView `
    display: flex;
    flex-direction: row;
    width: auto;
    padding: 0 0 0 4.1vw;
    margin: 0 0 2vh 0;

    overflow: auto;
    ::-webkit-scrollbar { display: none; }
`;

/*------------------------------------------------------*/

export const Recently = styled.View `
    display: flex;
    flex-direction: column;
    width: 91.79vw;
    margin: 4.5vh 0 4.5vh 0;
`;

/*------------------------------------------------------*/

export const ChosenSec = styled.View `
    display: flex;
    flex-direction: column;
    width: 91.79vw;
    margin: 0 0 15vh 0;
`;

export const ChosenItems = styled.View `
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;