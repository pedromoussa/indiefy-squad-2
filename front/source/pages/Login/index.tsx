import React from 'react';
import { Container, ContainerHeader, Slogan,
    Photo, Content, ButtonCad, TextLog,
    ButtonEntrar, TextAcesso} from './style';
import { useNavigation } from '@react-navigation/native';
import Previous from '../../components/Previous';

export default function Login() {

    const nav = useNavigation();

    return (
        <Container>
            <Previous/>
            <ContainerHeader>
                <Photo source={require('../../assets/LetLogo.svg')} />/

                <Slogan>
                    Bem-vindo(a) a uma experiência única{'\n'}
                    que une música e rede social
                </Slogan>
            </ContainerHeader>

         <Content>
            <ButtonCad style={{backgroundColor: '#ff420f'}}>
                <TextLog>Inscreva-se gratuitamente</TextLog>
            </ButtonCad>

            <ButtonCad>
                <TextLog>Continuar com o número de telefone</TextLog>
            </ButtonCad>

            <ButtonCad>
                <TextLog>Continuar com o Google</TextLog>
            </ButtonCad>

            <ButtonCad>
                <TextLog>Continuar com o Facebook</TextLog>
            </ButtonCad>

            <ButtonCad>
                <TextLog>Continuar com a Apple</TextLog>
            </ButtonCad>

            <ButtonEntrar onPress={() => nav.navigate('Profile' as never)}>
                <TextAcesso>Entrar</TextAcesso>
            </ButtonEntrar>
        </Content>
        </Container>
    );
};
