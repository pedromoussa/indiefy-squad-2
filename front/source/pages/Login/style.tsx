import styled from 'styled-components/native';
import { global } from '../../GlobalStyle';

export const Container = styled.View`
    background-color: black;
    width: 100vw;
    min-height: 100vh;
`;

export const ContainerHeader = styled.View`
    margin: 7.5vh 0 0 0;
    justify-content: space-between;
`;

export const Photo = styled.Image`
    height: 250px;
    width: 250px;
    align-self: center;
`;

export const Slogan  = styled.Text `
    font-family: ${global.fonts.poppins};
    font-size: 4.1vw;

    text-align: center;
    color:#fff;
    font-weight: bold;
`;

export const Content = styled.View`
    margin: 8vh 0 0 0;
    align-items:center;
`;

export const ButtonCad = styled.TouchableOpacity`
    width: 82.05vw;
    height: 5.5vh;

    margin: 2vh 0 0 0;

    justify-content: center;

    background-color: #232323;
    border-radius: 30px;
`;

export const TextLog = styled.Text`
    font-family: ${global.fonts.poppins};
    font-weight: 600;
    font-size: 3.07vw;
    text-align: center;
    color: #fff;
    font-size: 14px;
`;

export const ButtonEntrar = styled.TouchableOpacity``;

export const TextAcesso = styled.Text`
    margin-top: 20px;
    color: #fff;
    font-size: 18px;
    font-weight: bold;
`;

    