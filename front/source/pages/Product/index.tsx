import { Actions, Author, BotaoIcon, CommentDiv, CommentIcon, CommentTitle, Cover,
    Icon, Logo, Price, ProdDiv, ProdInfo,
    SongAlbum, Title } from "./style";

import { SectionTitle, SuggItems, Suggested } from "../Home/style";
import Previous from "../../components/Previous";
import MusicPreview from "../../components/MusicPreview";

import { useState } from "react";

import { global } from "../../GlobalStyle";

const commentIcon = require('../../assets/comentario.svg');

type prodInfo = {
    cover?: any;
    title?: string;
    price?: number
    album?: boolean;
    author?: string;
}
export default function Product(
    {cover, title, price, album, author}: prodInfo
) {

    const [fav, setFav] = useState(false);
    const [cart, setCart] = useState(false);    

    const AddFav = () => {
        if(fav) setFav(false);
        else setFav(true);
    }

    const AddCart = () => {
        if(cart) setCart(false);
        else setCart(true);
    }

    return (
        <ProdDiv>
            <Previous/>
            <Logo source={global.icons.logo} blurRadius={3}/>
            <ProdInfo>
                <Cover source={global.icons.coverPlaceholder}/>
                <SongAlbum>{`${album?'Álbum':'Música'}`}</SongAlbum>
                <Title>Planet Her{title}</Title>
                <Author>Doja Cat{author}</Author>
                <Price>R$ 10.99{price}</Price>

                <Actions>
                    <BotaoIcon onPress={AddFav}>
                        <Icon source={
                            fav? global.icons.faved
                                :global.icons.fav
                        }/>
                    </BotaoIcon>

                    <BotaoIcon onPress={AddCart}>
                        <Icon source={
                            cart? global.icons.addedCart
                                :global.icons.cart
                        }/>
                    </BotaoIcon>
                </Actions>
            </ProdInfo>

            <Suggested style={{backgroundColor: 'transparent', marginTop: '5vh'}}>
                <SectionTitle style={{marginLeft: '4.1vw'}}>Recomendados</SectionTitle>
                <SuggItems horizontal showsHorizontalScrollIndicator={false}>
                    <MusicPreview chosen/>
                    <MusicPreview chosen/>
                    <MusicPreview chosen/>
                    <MusicPreview chosen/>
                    <MusicPreview chosen/>
                    <MusicPreview chosen/>
                </SuggItems>
            </Suggested>

            <CommentDiv>
                <CommentTitle>Comentários</CommentTitle>
                <CommentIcon source={commentIcon}/>
            </CommentDiv>
        </ProdDiv>
    );
}