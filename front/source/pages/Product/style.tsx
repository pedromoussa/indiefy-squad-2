import styled from "styled-components/native";
import { global } from "../../GlobalStyle";

export const ProdDiv = styled.View `
    display: column;
    align-items: center;
    width: 100vw;
    height: 100vh;
    background: black;
`;

export const Logo = styled.Image `
    top: 19.66vh;
    position: absolute;

    width: 90vw;
    height: 15.2vh;
    filter: opacity(60%);
`;

/*------------------------------------------------------*/

export const ProdInfo = styled.View `
    display: column;
    align-items: center;
    margin: 7vh 0 0 0;
`;

export const Cover = styled.Image `
    margin: 7vh 0 0 0;
    width: 48.71vw;
    height: 48.71vw;
`;

export const SongAlbum = styled.Text `
    margin: 1vh 0;
    font-family: ${global.fonts.poppins};
    font-size: 3.33vw;
    font-weight: 500;
    color: ${global.colors.rosapink};
`;

export const Title = styled.Text `
    font-family: ${global.fonts.poppins};
    font-size: 6.15vw;
    font-weight: 500;
    color: white;
`;

export const Author = styled.Text `
    font-family: ${global.fonts.poppins};
    font-size: 3.33vw;
    font-weight: 500;
    color: white;
`;

export const Price = styled.Text `
    margin: 2vh 0 0 0;
    padding: 0.5vh 1.5vh;

    font-family: ${global.fonts.poppins};
    font-size: 5.12vw;
    font-weight: 500;

    color: white;
    background: ${global.colors.verde};

    border-radius: 0.7vh;
`;

/*------------------------------------------------------*/

export const Actions = styled.View `
    display: flex;
    flex-direction: row;
    width: 50%;
    margin: 2vh 0 0 0;
    justify-content: space-between;
`;

export const BotaoIcon = styled.TouchableOpacity `
    background: transparent;
    color: transparent;
`;

export const Icon = styled.Image `
    width: 6vw;
    height: 6vw;
`;

/*------------------------------------------------------*/

export const CommentDiv  = styled.TouchableOpacity `
    display: flex;
    flex-direction: row;

    margin: 4vh 0 0 0;

    align-items: center;
`;

export const CommentTitle = styled.Text `
    font-family: ${global.fonts.poppins};
    font-weight: 500;
    font-size: 5.12vw;

    color: white;
`

export const CommentIcon = styled.Image `
    width: 20%;
    height: 80%;
    margin: 0 0 0 2vw;
`;