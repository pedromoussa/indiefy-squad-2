import { useNavigation } from "@react-navigation/native";
import { global } from "../../GlobalStyle";
import Previous from "../../components/Previous";
import { Data, DataDiv, FollowDiv, Info, Logo, ProfileDiv,
    ProfileInfo, ProfilePic, PublishDiv, PublishIcon, PublishText, Settings } from "./style";

type userData = {
    userPic?: string;
    userName?: string;

    followers?: number;
    following?: number;
    listeners?: number;
}

export default function Profile(
    {userPic, userName, followers, following, listeners}:userData
) {

    const nav = useNavigation();

    return (
        <ProfileDiv>
                <Previous/>
                <Settings source={require('../../assets/settings.svg')}/>
            <Logo source={global.icons.logo}/>
            <ProfileInfo>
                <ProfilePic source={global.icons.coverPlaceholder}/>

                <FollowDiv>
                    <DataDiv>
                        <Data>1.000{followers}</Data>
                        <Info>Seguidores</Info>
                    </DataDiv>

                    <DataDiv>
                        <Data>20{following}</Data>
                        <Info>Seguindo</Info>
                    </DataDiv>
                </FollowDiv>

                <DataDiv>
                    <Data>1.000{listeners}</Data>
                    <Info>Ouvintes</Info>
                </DataDiv>
            </ProfileInfo>

            <PublishDiv onPress={() => nav.navigate('AddProduct' as never)}>
                <PublishIcon source={require('../../assets/publishSong.svg')}/>
                <PublishText>Publicar Música</PublishText>
            </PublishDiv>
        </ProfileDiv>
    );
}