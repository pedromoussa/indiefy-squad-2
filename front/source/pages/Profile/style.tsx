import styled from "styled-components/native";
import { global } from "../../GlobalStyle";

export const ProfileDiv = styled.View `
    display: column;
    width: 100vw;
    min-height: 100vh;
    align-items: center;
    background: black;
`;

export const Settings = styled.Image `
    width: 5.12vw;
    height: 5.12vw;

    position: absolute;
    top: 5vh;
    right: 7vw;
`;

export const Logo = styled.Image `
    width: 58.97vw;
    height: 10.07vh;
    margin: 5vh 0 0 0;
`;

/*------------------------------------------------------*/

export const ProfileInfo = styled.View `
    display: column;
    margin: 5vh 0 0 0;
    align-items: center;
`;

export const ProfilePic = styled.Image `
    width: 50vw;
    height: 50vw;

    border: solid 1vw ${global.colors.rosapink};
    border-radius: 50%;
`;

export const FollowDiv = styled.View `
    flex-direction: row;
    width: 100%;
    align-items: flex-end;
    justify-content: space-between;
`;

export const DataDiv = styled.View `
    flex-direction: row;
    margin: 2vh 0 0 0;
    align-items: center;
`;

export const Data = styled.Text `
    margin: 0 1vw 0 0;
    font-family: ${global.fonts.vietnam};
    font-weight: 600;
    font-size: 4vw;
    color: ${global.colors.laranja};
`;

export const Info = styled.Text `
    font-family: ${global.fonts.vietnam};
    font-weight: 600;
    font-size: 3vw;
    color: white;
`;

/*------------------------------------------------------*/

export const PublishDiv = styled.TouchableOpacity `
    flex-direction: row;
    margin: 5vh 0 0 0;
`;

export const PublishIcon = styled.Image `
    width: 8.97vw;
    height: 3.04vh;
`;

export const PublishText = styled.Text `
    font-family: ${global.fonts.vietnam};
    font-weight: 600;
    font-size: 3vw;
    color: white;
`;