import { TouchableOpacity } from "react-native";
import Loading from "../../components/Loading";
import { SplashDiv, SplashLogo } from "./style";
import { useNavigation } from "@react-navigation/native";

const logo = require('../../assets/LetLogo.svg');

export default function Splash() {

    const nav = useNavigation();

    return (
        <SplashDiv>
            <TouchableOpacity onPress={() => nav.navigate('Home' as never)}>
                <SplashLogo source={logo}/>
            </TouchableOpacity>
            <Loading/>
        </SplashDiv>
    );
}