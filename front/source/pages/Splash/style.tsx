import styled from "styled-components/native";

export const SplashDiv = styled.View `
    display: flex;
    flex-direction: column;
    width: 100vw;
    height: 100vh;

    align-items: center;

    color: white;
    background: black;
`;

export const SplashLogo = styled.Image `
    display: flex;
    width: 95vw;
    height: 95vw;
    margin: 10vh 0 5vh 0;
`;