import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from '../pages/Home';
import AddProduct from '../pages/AddProduct';
import Splash from '../pages/Splash';
import Profile from '../pages/Profile';
import Login from '../pages/Login';
import Product from '../pages/Product';

export default function Router() {

    const Stack = createNativeStackNavigator();

    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName='Splash'
            screenOptions={{
                headerShown: false,
            }}>
                <Stack.Screen name='Splash' component={Splash} options={{}}/>
                <Stack.Screen name='Home' component={Home}/>
                <Stack.Screen name='Product' component={Product}/>
                <Stack.Screen name='Login' component={Login}/>
                <Stack.Screen name='Profile' component={Profile}/>
                <Stack.Screen name='AddProduct' component={AddProduct}/>                
            </Stack.Navigator>
        </NavigationContainer>
    );
}